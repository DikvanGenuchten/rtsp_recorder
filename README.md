# RTSP recorder

Record a live RTSP stream to video files.

This project was developed for use on BalenaOS devices. However it should work on any platform where a docker engine is available.

## Usage

### Fetch

1. On the balena device dashboard, open a "Host" terminal.

2. Change to a writable directory, e.g. `/mnt/data` or `/tmp`.

```cli
> cd /mnt/data
```

3. Download source

```cli
> wget https://gitlab.com/pgils/rtsp_recorder/-/archive/main/rtsp_recorder-main.tar.gz
```

4. Extract

```cli
> tar -xf rtsp_recorder-main.tar.gz
```

### Build

```cli
> cd rtsp_recorder-main
> balena build -t rtsp-rec:1 .
```

### Run

#### Options

```text
❯ ./record.sh -h
  usage: record.sh [OPTIONS] RTSP_SOURCE
    options
      -p, --part-length     Video part length in seconds (Default: 60)
      -a, --min-available   Minimum disk space to leave available in MB.
      -u, --max-used        Maximum disk space for recording in MB.
      -t, --time            Record time in minutes.
```

The default behaviour is to record indefinetly. This can be limited by using the optional
arguments `-a`, `-u`, and `-t`.

> Using these limiting options is recommended. Especially when recording on a device with a single partition.

```cli
> balena run -t \
    -v video-rec:/video \
    rtsp-rec:1 \
        rtsp://user:pass@ip-address/etc
```

> to run the container in the background, add a `-d` somewhere.

- `video-rec` is a local named volume. A directory can be used here as well.

#### Typical usage examples

##### Record for a limited time

Record for 10 minutes

```cli
> balena run -ti -v video-rec:/video rtsp-rec:1 \
    -t 10 \
    rtsp://user:pass@ip-address/mystream
```

##### Record for a limited time or disk usage

Record for 10 minutes **OR** until 25 MB of disk space has been used.

```cli
> balena run -ti -v video-rec:/video rtsp-rec:1 \
    -t 10 -u 25 \
    rtsp://user:pass@ip-address/mystream
```

##### Record until limited disk space is available

Record until 5 GB (5000 MB) of disk space remains.

```cli
> balena run -ti -v video-rec:/video rtsp-rec:1 \
    -a 5000 \
    rtsp://user:pass@ip-address/mystream
```

### Uploading video files

The `aws` utility is included and can be used to upload files to S3.

> AWS credentials can be set on launch using environment variables (`-e`) but can also
> be done when needed after recording (no auto upload is implemented)
>
> The relevant variables are:
>
> - AWS_REGION
> - AWS_ACCESS_KEY_ID
> - AWS_SECRET_ACCESS_KEY
